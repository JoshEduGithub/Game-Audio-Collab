using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
    }

    public void play()
    {
        SceneManager.LoadScene("MainLevel");

    }
    
    public void Exit()
    {
        Application.Quit();
    }

}
