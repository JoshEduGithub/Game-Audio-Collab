using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerFootsteps : MonoBehaviour {

    private enum CURRENT_TERRAIN { SAND, GRASS, WOOD, ROCK, WATER};

    [SerializeField]
    private CURRENT_TERRAIN currentTerrain;

    private FMOD.Studio.EventInstance footsteps;

    private void Update()
    {
        DetermineTerrain();
    }

    private void DetermineTerrain()
    {
        RaycastHit[] hit;

        hit = Physics.RaycastAll(transform.position, Vector3.down, 5.0f);

        foreach (RaycastHit rayhit in hit)
        {
            if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Grass"))
            {
                currentTerrain = CURRENT_TERRAIN.GRASS;
                break;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Wood"))
            {
                currentTerrain = CURRENT_TERRAIN.WOOD;
                break;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Sand"))
            {
                currentTerrain = CURRENT_TERRAIN.SAND;
                
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Rock"))
            {
                currentTerrain = CURRENT_TERRAIN.ROCK;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Water"))
            {
                currentTerrain = CURRENT_TERRAIN.WATER;
            }
        }
    }

    public void SelectAndPlayFootstep()
    {     
        switch (currentTerrain)
        {
            case CURRENT_TERRAIN.GRASS:
                PlayFootstep(0);
                //Debug.Log("GRASS");
                break;

            case CURRENT_TERRAIN.SAND:
                PlayFootstep(1);
                //Debug.Log("SAND");
                break;

            case CURRENT_TERRAIN.WOOD:
                PlayFootstep(2);
                //Debug.Log("Wood");
                break;

            case CURRENT_TERRAIN.ROCK:
                PlayFootstep(3);
                //Debug.Log("Rock");
                break;
                
            case CURRENT_TERRAIN.WATER:
                PlayFootstep(4);
                //Debug.Log("Water");
                break;

            default:
                PlayFootstep(0);
                break;
        }
    }

    private void PlayFootstep(int terrain)
    {
        footsteps = FMODUnity.RuntimeManager.CreateInstance("event:/Footsteps");
        footsteps.setParameterByName("Terrain", terrain);
        footsteps.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        footsteps.start();
        footsteps.release();
    }
}
