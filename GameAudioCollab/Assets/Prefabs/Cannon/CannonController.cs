using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FMODUnity;
using FMOD.Studio;
public class CannonController : MonoBehaviour
{
    [SerializeField]
    public UnityEvent InteractCannon;
    public GameObject cannonBall;
    public Transform CannonBarrel;
    public float force;
    

    public void FireCannon()
    {
        GetComponent<FMODUnity.StudioEventEmitter>().Play();
        //Debug.Log("AudioPlayed");
        StartCoroutine(Delay(1f));

        
    }

    IEnumerator Delay(float timedelay)
    {
        yield return new WaitForSeconds(timedelay);

        GameObject Projectile = Instantiate(cannonBall, CannonBarrel.position, CannonBarrel.rotation);
        Projectile.GetComponent<Rigidbody>().velocity = CannonBarrel.forward * force * Time.deltaTime;
        //Debug.Log("projectile fired");

    }


    
    

    



    
    
}
