using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractScript : MonoBehaviour
{

    public LayerMask interactableLayerMask = 8;
    UnityEvent Interact;
    float TimeGone;

    public Camera main;
    void start()
    {
        TimeGone = Time.time;
    }

    void Update()
    {
        RaycastHit hit;

        if(Physics.Raycast(main.transform.position,main.transform.forward, out hit,2,interactableLayerMask))
        {
            if (hit.collider.GetComponent<CannonController>() != false)
            {
                Interact = hit.collider.GetComponent<CannonController>().InteractCannon;
                if(Input.GetKeyDown(KeyCode.E) && (Time.time - TimeGone> 5.0f))
                {
                    Interact.Invoke();
                    TimeGone = Time.time;
                }

            }
            //Debug.Log(hit.collider.name);
        }

        if(Physics.Raycast(main.transform.position,main.transform.forward, out hit,2,interactableLayerMask))
        {
            if (hit.collider.GetComponent<RainLightSwitch>() != false)
            {
                Interact = hit.collider.GetComponent<RainLightSwitch>().LightSwitch;
                if(Input.GetKeyDown(KeyCode.E) && (Time.time - TimeGone> 5.0f))
                {
                    Interact.Invoke();
                    TimeGone = Time.time;
                }

            }
            //Debug.Log(hit.collider.name);
        }
    }
}
    

    
