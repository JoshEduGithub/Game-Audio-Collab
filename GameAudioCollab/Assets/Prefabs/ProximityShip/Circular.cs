using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circular : MonoBehaviour
{
    private float _timeCounter;

    public float _speed;
    public float _width;
    public float _height;


    void Update()
    {
        _timeCounter += Time.deltaTime*_speed;

        float x = Mathf.Cos (_timeCounter)*_width;
        float y = 10;
        float z = Mathf.Sin (_timeCounter)*_height;

        transform.position = new Vector3(x,y,z);
        
        
    }



}

