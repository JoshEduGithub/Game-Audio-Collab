using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FMODUnity;
using FMOD.Studio;

public class RainLightSwitch : MonoBehaviour
{
    [SerializeField]
    public UnityEvent LightSwitch;
    public GameObject FireParticle;
    public GameObject rainParticle;
    public StudioEventEmitter rain;
    private bool x = false;

    public void _lightSwitch()
    {
        if (x == false)
        {
            rain.Play();
            Debug.Log("LightSwitchedOn");
            FireParticle.SetActive(true);
            rainParticle.SetActive(true);
            x = true;
        }
        else if (x == true)
        {
            rain.Stop();
            Debug.Log("LightSwitchedOff");
            FireParticle.SetActive(false);
            rainParticle.SetActive(false);
            x = false;

        }

        
    }
}
