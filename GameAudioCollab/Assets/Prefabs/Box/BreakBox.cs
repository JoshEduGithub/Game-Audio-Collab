using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakBox : MonoBehaviour
{
    
    public UserInterface Score;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
            Score.ScoreCount(1);
            Destroy (gameObject);

            
        }
    }
}
