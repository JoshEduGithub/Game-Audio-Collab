using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Doorway : MonoBehaviour
{
    public GameObject GameWinPanel;
    void OnTriggerEnter(Collider col)
    {
        GameWinPanel.SetActive(true);
        StartCoroutine(levelLoadDelay());
        Cursor.visible = true;
        
    }

    IEnumerator levelLoadDelay()
    {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("MainMenuScene");
    }
}
