using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;
using FMODUnity;

public class UserInterface : MonoBehaviour
{

    public static bool isPaused = false;
    public GameObject PauseMenuGUI;
    public GameObject PauseMOptions;
    public GameObject cam;
    static float CurrentScore;
    public TextMeshProUGUI ScoreText;
    public GameObject door;

    
    void Awake()
    {
        Cursor.visible = false;
        CurrentScore = 0;
        isPaused = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            if (isPaused == true)
            {
                Continue();
                PauseMOptions.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = false;
                

                
            }
            else
            {
                Paused();
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }
        
    }


    private void Continue()
    {
        PauseMenuGUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
        


    }
    private void Paused()
    {
        PauseMenuGUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        

    }

    

     public void ContinueBTN()
    {
        Continue();
    }

    public void OptionsBtn()
    {
        PauseMOptions.SetActive(true);
        PauseMenuGUI.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ReturnToOptionsMenu()
    {
        PauseMOptions.SetActive(false);
        PauseMenuGUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ReturnTOMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
        Time.timeScale = 1f;
     
    }

public void ScoreCount(int Score)
    {
        CurrentScore+= Score;
        
        ScoreText.text = "" + CurrentScore;
        Debug.Log(CurrentScore);

        if (CurrentScore == 20)
        {
           
           door.SetActive(true);
        }

    }

    
}