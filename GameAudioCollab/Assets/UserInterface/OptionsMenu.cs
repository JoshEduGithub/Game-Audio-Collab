using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{

    FMOD.Studio.Bus BackgroundMusic;
    FMOD.Studio.Bus SFX;
    FMOD.Studio.Bus Master;

    float musicV = 0.5f;
    float sfxV = 0.5f;
    float masterV = 1f;


    void Awake()
    {
        BackgroundMusic = FMODUnity.RuntimeManager.GetBus("bus:/MasterController/Music");
        SFX = FMODUnity.RuntimeManager.GetBus("bus:/MasterController/SFX");
        Master = FMODUnity.RuntimeManager.GetBus("bus:/MasterController");
    }

    void Update ()
    {
        BackgroundMusic.setVolume (musicV);
        SFX.setVolume (sfxV);
        Master.setVolume (masterV);

    }

    public void MasterVolumeControl (float Masterx)
    {
        masterV = Masterx;
    }

    public void MusicVolumeControl (float Musicx)
    {
        musicV = Musicx;
    }

    public void SFXVolumeControl (float SFXx)
    {
        sfxV = SFXx;
    }

}
